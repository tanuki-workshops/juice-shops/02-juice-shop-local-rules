#!/bin/bash

curl https://semgrep.dev/c/p/owasp-top-ten --output owasp-top-ten.yml
curl https://semgrep.dev/c/p/jwt --output jwt.yml
curl https://semgrep.dev/c/p/command-injection --output command-injection.yml
curl https://semgrep.dev/c/p/javascript --output javascript.yml
curl https://semgrep.dev/c/p/xss --output xss.yml
curl https://semgrep.dev/c/p/react --output react.yml
curl https://semgrep.dev/c/p/r2c-security-audit --output r2c-security-audit.yml
curl https://semgrep.dev/c/p/cwe-top-25 --output cwe-top-25.yml
curl https://semgrep.dev/c/p/insecure-transport --output insecure-transport.yml
curl https://semgrep.dev/c/p/security-audit --output sec-audit.yml
curl https://semgrep.dev/c/p/secrets --output secrets.yml
curl https://semgrep.dev/c/p/ci --output ci.yml
curl https://semgrep.dev/c/p/xss --output xss.yml
